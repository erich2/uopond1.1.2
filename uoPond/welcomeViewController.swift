//
//  welcomeViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 1/12/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit

class welcomeViewController: UIViewController {

    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var enjoy: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enjoy.layer.cornerRadius = 10
        boxView.layer.cornerRadius = 10
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        boxView.layer.backgroundColor = UIColor.black.withAlphaComponent(0.75).cgColor
        self.enjoy.layer.borderWidth = 1
        self.enjoy.layer.borderColor = UIColor.systemBlue.cgColor
    }
    
    @IBAction func proceed(_ sender: Any) {
        dismissAnimate()
    }
    
    func dismissAnimate(){
           UIView.animate(withDuration: 0.25, animations: {
               self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
               self.view.alpha = 0.0;
           }) { (finished) in
               if (finished){
                   self.view.removeFromSuperview()
               }
           }
       }

}
