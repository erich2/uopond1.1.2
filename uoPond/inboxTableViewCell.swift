//
//  inboxTableViewCell.swift
//  uoPond
//
//  Created by Ethan Richards on 7/13/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit

class inboxTableViewCell: UITableViewCell {

    @IBOutlet weak var latestText: UILabel!
    @IBOutlet weak var offerTitle: UILabel!
    
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var readLabel: UILabel!
    
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var postTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
        self.userImage.layer.cornerRadius = self.userImage.frame.size.width / 2
        self.userImage.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    
    
    
    
    func setUpView(){
        let seperator = UIView()
        seperator.translatesAutoresizingMaskIntoConstraints = false
        seperator.backgroundColor = UIColor.lightGray
        self.addSubview(seperator)
        seperator.leftAnchor.constraint(equalTo: self.leftAnchor, constant: -8).isActive = true
        seperator.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        seperator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        seperator.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
}
