//
//  messageTableViewCell.swift
//  uoPond
//
//  Created by Ethan Richards on 1/29/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit

class messageTableViewCell: UITableViewCell {

    var textMess:String!
    var mine:Bool!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.setUpMess()
            print("set up : ", self.textMess)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text

        label.sizeToFit()
        return label.frame.height
    }
    
    func widthForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
           let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
           label.numberOfLines = 0
           label.lineBreakMode = NSLineBreakMode.byWordWrapping
           label.font = font
           label.text = text

           label.sizeToFit()
           return label.frame.width
       }
    
    
    
    func setUpMess(){
        let messLabel = UITextView(frame: CGRect(x: (UIScreen.main.bounds.width - widthForView(text: self.textMess, font: UIFont.systemFont(ofSize: 17), width: 150) - 10), y: 0, width: 10 + widthForView(text: self.textMess, font: UIFont.systemFont(ofSize: 17), width: 150), height: 20 + heightForView(text: self.textMess, font: UIFont.systemFont(ofSize: 17), width: 150)))
        print("width:", widthForView(text: self.textMess, font: UIFont.systemFont(ofSize: 17), width: 150))
        print("height:", heightForView(text: self.textMess, font: UIFont.systemFont(ofSize: 17), width: 150))
        messLabel.textAlignment = .left
        //messLabel.numberOfLines = 0
        messLabel.layer.cornerRadius = 15
        messLabel.isEditable = false
        messLabel.isSelectable = false
        //messLabel.lineBreakMode = .byWordWrapping
        messLabel.font = UIFont.systemFont(ofSize: 17)
        messLabel.text = self.textMess!
        messLabel.layer.cornerRadius = 10
        self.addSubview(messLabel)
        
        
        
        if self.mine == true{
            messLabel.textColor = UIColor.white
            messLabel.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: 10).isActive = true
            messLabel.backgroundColor = UIColor.systemBlue
            messLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        }else if self.mine == false{
            messLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 10).isActive = true
            messLabel.textColor = UIColor.black
            messLabel.backgroundColor = UIColor.systemGray
            messLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        }
    }

}
