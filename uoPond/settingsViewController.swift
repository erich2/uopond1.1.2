//
//  settingsViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 4/26/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class settingsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var changePassword: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var viewWidth: NSLayoutConstraint!
    
    var uid:String!
    var email = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.presentAnimate()
        self.maybeAnimate()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.changePassword.layer.cornerRadius = 10
        self.saveButton.layer.cornerRadius = 10
        self.updateButton.layer.cornerRadius = 10
        self.profilePic.image = #imageLiteral(resourceName: "Default-welcomer")
        self.navigationItem.title = "Settings"
        self.uid = Auth.auth().currentUser!.uid
        let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
        let userReference = ref.child(uid)
        userReference.observe( .value) { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            let userEmail = value?["email"] as? String ?? ""
            self.email = [userEmail]
        }
        //let swipe = UIGestureRecognizer(target: self, action: #selector(self.swipeRight(_:)))
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func swipeRight(_ sender: Any) {
        self.dismissAnimate()
        
    }
    
    @IBAction func signOut(_ sender: Any) {
        self.performSegue(withIdentifier: "signedOut", sender: self)
        self.showSpinner(onView: self.view)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "signedOut"{
            do{
                try Auth.auth().signOut()
                self.removeSpinner()
            }catch{
                print("error signing out")
                self.removeSpinner()
            }
        }
    }
    
   
    
    @IBAction func savePressed(_ sender: Any) {
        if self.profilePic.image! != #imageLiteral(resourceName: "Default-welcomer") {
            
            guard let image = self.profilePic.image else{
                print("error")
                return
            }
            let imageName = UUID().uuidString
            let data = UIImageJPEGRepresentation(image, 1.0)
            let imageRef = Storage.storage().reference().child("profPics").child("\(imageName).jpg")
            imageRef.putData(data!, metadata: nil) { (metadata, error) in
                if error != nil {
                    print(error!)
                    return
                }else{
                    imageRef.downloadURL(completion: { (url, err) in
                        if err != nil{
                            print(err!)
                            return
                        }else{
                            guard let url = url else{
                                print("unsuccessful")
                                return
                            }
                            let urlString = url.absoluteString
                            let uid = Auth.auth().currentUser!.uid
                            let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
                            let userReference = ref.child(uid)
                            let values = ["profilePicture" : urlString]
                            userReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
                                if err != nil{
                                    print(err!)
                                    return
                                }else{
                                    print("success")
                                }
                            })
                        }
                    })
                }
                
            }
        }
    }
    
   
    @IBAction func updatePressed(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage{
            selectedImage = editedImage
        }else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImage = originalImage
        }
        self.profilePic.image = selectedImage
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    @IBAction func changePWord(_ sender: Any) {
        Auth.auth().sendPasswordReset(withEmail: self.email[0]) { (err) in
            if err != nil {
                print(err!)
                return
            }else{
                let alertController = UIAlertController(title: "Sent!", message: "Please check your email to reset your password.", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    
   func presentAnimate(){
    UIView.animate(withDuration: 0.5, delay: 0.25, options: .transitionCrossDissolve, animations: { () -> Void in
        self.view.frame = CGRect(x: 0, y: self.view.frame.height ,width: 0,height: 0)
          }, completion: { (finished: Bool) -> Void in
//            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
          })
    }
    
    func dismissView(){
        func presentAnimate(){
            UIView.animate(withDuration: 0.5, delay: 0.25, options: .transitionCrossDissolve, animations: { () -> Void in
                //self.view.frame = CGRect(x: 0, y: 0 ,width: 0,height: 0)
                self.view.frame.origin.y -= 400
                  }, completion: { (finished: Bool) -> Void in
                    self.view.removeFromSuperview()
                  })
            }
    }
   
    func dismissAnimate() {

        let tr = CATransition()
        tr.duration = 0.15
        tr.type = kCATransitionReveal // use "Reveal" here
        tr.subtype = kCATransitionFromLeft
        view.window!.layer.add(tr, forKey: kCATransition)
        self.view.backgroundColor = UIColor.black

        self.view.removeFromSuperview()
    }
    
    func maybeAnimate(){
        let tr = CATransition()
        tr.duration = 0.15
        tr.type = kCATransitionReveal // use "Reveal" here
        tr.subtype = kCATransitionFromRight
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeSB") as! homeViewController
        vc.view.window!.layer.add(tr, forKey: kCATransition)
        self.view.backgroundColor = UIColor.black
    }
    
}


