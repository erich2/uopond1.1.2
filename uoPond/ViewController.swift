//
//  ViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 4/18/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class ViewController: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var registerButton: UIButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.layer.cornerRadius = 10
        registerButton.layer.cornerRadius = 10
        
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil{
                self.performSegue(withIdentifier: "currentUser", sender: self)
            }else{
                print("new user/user not logged in.")
        }
 
        }
 
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "currentUser"{
            let vc = segue.destination as! UINavigationController
            let target = vc.topViewController as! homeViewController
            target.userUid = Auth.auth().currentUser?.uid
        }
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    @IBAction func loginPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "toLogin", sender: self)
    }
    
    @IBAction func registerPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "toRegister", sender: self)
    }
    
    func displayDefaultAlert(title: String?, message: String?){
    
        let alert = UIAlertController(title:title, message:message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
        
        
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

