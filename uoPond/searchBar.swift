//
//  searchBar.swift
//  uoPond
//
//  Created by Ethan Richards on 6/11/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit

class SearchBar: UISearchBar {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
