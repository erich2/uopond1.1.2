//
//  loginViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 4/19/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class loginViewController: UIViewController {

    
    @IBOutlet weak var usernameBox: UITextField!
    
    @IBOutlet weak var loginSuccess: UILabel!
    @IBOutlet weak var passwordBox: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
   
    @IBOutlet weak var toRegister: UIButton!
    
    @IBOutlet weak var homeButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homeButton.layer.cornerRadius = 10
        toRegister.layer.cornerRadius = 10
        loginButton.layer.cornerRadius = 10
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped(sender:)))
        self.view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func tapped(sender: UIGestureRecognizer) {
        self.view.endEditing(true)
        print("tapped")
    }
    
    @IBAction func toRegister(_ sender: Any) {
        self.performSegue(withIdentifier: "loginToRegister", sender: self)
    }
    
    
    @IBAction func signInPressed(_ sender: Any) {
        if (passwordBox.text == "") || (usernameBox.text == ""){
            let alertController = UIAlertController(title: "Oops", message: "Please fill out all fields", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        }else{
            self.logIn()
            
        }
    }
    
    

        
        
    
    func logIn(){
        self.showSpinner(onView: self.view)
        Auth.auth().signIn(withEmail: usernameBox.text!, password: passwordBox.text!) { (signin, error) in
            if error == nil{
                self.loginButton.isHidden = true
                self.usernameBox.isHidden = true
                self.passwordBox.isHidden = true
                self.loginSuccess.isHidden = false
                self.removeSpinner()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.performSegue(withIdentifier: "loginToHome", sender: self)
                })
                
                return
            }else{
                self.removeSpinner()
                let alertController = UIAlertController(title: "Unable to Sign in.", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loginToHome"{
            let vc = segue.destination as! UINavigationController
            let target = vc.topViewController as! homeViewController
            target.userUid = Auth.auth().currentUser?.uid
        }else if segue.identifier == "loginToMarket" {
            let vc = segue.destination as! UINavigationController
            let target = vc.topViewController as! marketTableViewController
            target.myID = Auth.auth().currentUser?.uid
        }
    }
    
    
    @IBAction func homePressed(_ sender: Any) {
        self.performSegue(withIdentifier: "loginToHome", sender: self)
    }
    
}






























