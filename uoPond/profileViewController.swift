//
//  profileViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 4/19/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth

class profileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var naviBar: UINavigationBar!
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var firstNameBox: UITextField!
    @IBOutlet weak var lastNameBox: UITextField!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var venmoBox: UITextField!
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    var UID:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.naviBar.topItem?.title = "One More Step"
        // THE ERRROR NEED TO UID print("THE UID: ",self.userUid!)
        self.doneButton.layer.cornerRadius = 10
        self.profilePic.image = #imageLiteral(resourceName: "Default-welcomer")
        self.profilePic.layer.cornerRadius = self.profilePic.frame.size.width / 2
        self.profilePic.clipsToBounds = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped(sender:)))
        self.view.addGestureRecognizer(tap)
    }

    @IBAction func endEditing(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func uploadPressed(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    
    @objc func tapped(sender: UIGestureRecognizer) {
           self.view.endEditing(true)
           print("tapped")
       }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage{
            selectedImage = editedImage
        }else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImage = originalImage
        }
        self.profilePic.image = selectedImage
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
   
    
    @IBAction func donePressed(_ sender: Any) {
   
        if self.lastNameBox.text == "" || self.firstNameBox.text == "" || self.profilePic == #imageLiteral(resourceName: "Default-welcomer") || self.venmoBox.text == ""{
            
            let alertController = UIAlertController(title: "Oops", message: "Please fill out all fields", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
           
        }else{
            print("right before animating")
            //self.showSpinner(onView: self.view)
            guard let image = self.profilePic.image else{
                print("error")
                return
            }
            //let imageName = UUID().uuidString
            guard let uid = self.UID else{
                print("error on uid")
                return
            }
            let data = UIImageJPEGRepresentation(image, 1.0)
            let imageRef = Storage.storage().reference().child("users").child(self.UID!).child("profPicture")
            imageRef.putData(data!, metadata: nil) { (metadata, error) in
                if error != nil {
                    print(error!)
                    return
                }else{
                    imageRef.downloadURL(completion: { (url, err) in
                        if err != nil{
                            print(err!)
                            return
                        }else{
                            /*guard let url = url else{
                                print("unsuccessful")
                                return
                            }*/
                            //let urlString = String(describing: url.absoluteURL)
                            
                            //let urlString = "gs://uopond.appspot.com/profPics/\(imageName)/"
                            //print("URL", urlString)
                            let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/")
                            let userReference = ref.child("Users").child(uid)
                            let values = ["firstName" : self.firstNameBox.text!, "lastName" : self.lastNameBox.text!, "venmo" : self.venmoBox.text!]
                            print("BEFORE UPDATING VALUES")
                            userReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
                                if err != nil{
                                    print(err!)
                                    return
                                }else{
                                    print("success")
                                }
                            })
                        }
                        print("AFTER VALUES SHOULD HAVE UPDATED")
                    })
                }
             //   self.removeSpinner()
            }
            self.doneButton.isHidden = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.performSegue(withIdentifier: "user2Home", sender: self)
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "user2Home"{
            let vc = segue.destination as! UINavigationController
            let target = vc.topViewController as! homeViewController
            target.userUid = self.UID!
            target.welcomeUser()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

