//
//  previewViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 1/28/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class previewViewController: UIViewController {

    @IBOutlet weak var viewTag: UIView!
    
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var descripBox: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descripToggle: UIButton!
    var post:tempPost!
    var uid:String!
    var userName:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.uid = Auth.auth().currentUser?.uid
        self.priceLabel.text = self.post.price
        self.descripBox.text = self.post.descrip
        self.titleLabel.text = self.post.title
        self.postImage.image = self.post.image
        self.userName = self.post.userName
        self.navigationItem.title = "Preview"
        self.setUpImage()
        self.setUpDescrip()
        self.setUpPriceandViewTag()
        self.setUpToolbar()
    
    }
    
    func setUpDescrip(){
        self.descripBox.numberOfLines = 0
        self.descripBox.backgroundColor = UIColor.black.withAlphaComponent(0.40)
        self.descripBox.lineBreakMode = NSLineBreakMode.byTruncatingTail
        
    }
    
    func setUpToolbar(){
        let post = UIBarButtonItem(title: "publish", style: .plain, target: self, action: #selector(publish))
        let rS = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let lS = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        
        self.toolbarItems = [lS,post,rS]
    }
    
    @objc func publish(){
        
        guard let image = self.postImage.image else{
                       print("error")
                       return
                   }
        
        self.showSpinner(onView: self.view)
        let imageName = UUID().uuidString
        let data = UIImageJPEGRepresentation(image, 1.0)
        let imageRef = Storage.storage().reference().child("Posts").child("\(imageName).jpg")
        imageRef.putData(data!, metadata: nil) { (metadata, error) in
            if error != nil {
                print(error!)
                print("ERROR")
                return
            }else{
                imageRef.downloadURL(completion: { (url, err) in
                    if err != nil{
                        print(err!)
                        return
                    }else{
                        guard let url = url else{
                            print("unsuccessful")
                            return
                        }
                        let urlString = url.absoluteString
                        let postNumber = UUID().uuidString
                        let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts")
                        let userReference = ref.child("Post\(postNumber)")
                        let values = ["title" : self.titleLabel.text!,
                                      "description" : self.descripBox.text!,
                                      "price" : self.priceLabel.text!,
                                      "image" : urlString,
                                      "user" : self.uid,
                                      "userName" : self.userName!,
                                      "postID" : postNumber]
                        userReference.setValue(values, withCompletionBlock: { (err, ref) in
                            if err != nil{
                                print(err!)
                                print("ERROR")
                                self.removeSpinner()
                                return
                            }else{
                                print("success")
                                self.removeSpinner()
                                self.performSegue(withIdentifier: "publishComplete", sender: self)
                            }
                        })
                    }
                })
            }
            
        }
        
    }
    
    func setUpPriceandViewTag(){
        viewTag.backgroundColor = UIColor.black.withAlphaComponent(0.40)
        self.priceLabel.adjustsFontSizeToFitWidth = true
        
    }
    
    func setUpImage(){
        postImage.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(sender:)))
        postImage.addGestureRecognizer(tap)
    }
    
    @available(iOS 13.0, *)
    @IBAction func descripExpanded(_ sender: Any) {
        if self.descripBox.isHidden == true{
            self.descripBox.isHidden = false
            self.descripToggle.setImage(UIImage(systemName: "chevron.up"), for: .normal)
            
        }else{
            self.descripBox.isHidden = true
            self.descripToggle.setImage(UIImage(systemName: "chevron.down"), for: .normal)

        }
    }
    
   
    
    @objc func imageTapped(sender: UITapGestureRecognizer){
           if viewTag.isHidden == true{
               viewTag.isHidden = false
               
           }else{
               viewTag.isHidden = true
               
           }
       }
     
    
}
